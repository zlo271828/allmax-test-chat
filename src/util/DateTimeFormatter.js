const getFormattedDateForChat = (date) => date.toLocaleString();

export {getFormattedDateForChat};