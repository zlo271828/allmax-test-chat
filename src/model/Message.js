import mongoose from 'mongoose';

const MessageSchema = new mongoose.Schema({
    user: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    date: {
        type: Number,
        required: true,
        get: (date) => new Date(date),
        set: (date) => date.getTime()
    }
});

export default mongoose.model('Message', MessageSchema);