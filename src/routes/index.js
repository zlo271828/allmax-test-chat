import express from 'express';
import logger from '../util/logger';
import Message from '../model/Message';

import messages from './chat';

const router = express.Router();
const clients = [];

async function precessMessage(message) {
    try {
        const newMessage = new Message();
        newMessage.text = message.message;
        newMessage.date = new Date();
        newMessage.user = message.user;
        await newMessage.save();
    } catch (error) {
        console.log(error);
    }
}

export default (io) => {

    router.get('/', (req, res) => res.render('index'));

    router.use('/chat', messages);

    router.get('/clients', (req, res) => res.json(clients));

    io.on('connection', (socket) => {
        if (!clients.includes(socket.request.connection.remoteAddress))
            clients.push(socket.request.connection.remoteAddress);
        logger.info(`Client connected`);
        socket.broadcast.emit('info-message', 'New client in the chat');
        socket.on('disconnect', () => {
            if (clients.includes(socket.request.connection.remoteAddress))
                clients.splice(clients.indexOf(socket.request.connection.remoteAddress), 1);
            logger.info('Client disconnected')
        });
        socket.on('send-message', (message) => {
            const messageObj = {message, user: socket.request.connection.remoteAddress};
            io.emit('receive-message', messageObj);
            precessMessage(messageObj);
        });
    });

    return router;
};
